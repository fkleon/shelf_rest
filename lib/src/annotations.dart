// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.resource.annotation;
import 'package:shelf_bind/shelf_bind.dart';
import 'package:shelf/shelf.dart';

class RestResource {
  final String pathParameterName;

  const RestResource(this.pathParameterName);
}

class RestOperation {
  final String defaultMethodName;
  final String defaultHttpMethod;
  final ResponseHeaders responseHeaders;

  const RestOperation._internal(this.defaultMethodName,
      this.defaultHttpMethod, [this.responseHeaders = const ResponseHeaders()]);

  static const RestOperation CREATE = const RestOperation._internal('create',
      'POST', const ResponseHeaders.created());
  static const RestOperation UPDATE = const RestOperation._internal('update',
      'PUT');
  static const RestOperation FIND = const RestOperation._internal('find',
      'GET');
  static const RestOperation SEARCH = const RestOperation._internal('search',
      'GET');
  static const RestOperation DELETE = const RestOperation._internal('delete',
      'DELETE');

  static const List<RestOperation> ALL =
      const [CREATE, UPDATE, FIND, SEARCH, DELETE];

  static RestOperation defaultOperationForMethodName(String methodName) {
    return ALL.firstWhere((op) => op.defaultMethodName == methodName,
        orElse: () => null);
  }
}

typedef Middleware _MiddlewareFactory();

class ResourceMethod {
  final String method;
  final RestOperation operation;
  final _MiddlewareFactory middleware;
  final bool validateParameters;
  final bool validateReturn;

  const ResourceMethod({ this.method, this.operation,
    this.middleware, this.validateParameters,
    this.validateReturn });
}

