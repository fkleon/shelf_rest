## 0.1.3

* Allow validation of parameters to be configured at the bind function and
annotation level

## 0.1.2+1

* Doco improvements

## 0.1.2

* Turned validation of parameters on by default

## 0.1.1

* Add middleware support in the `RestMethod` annotation

## 0.1.0

* First version
